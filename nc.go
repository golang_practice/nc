package main

import (
	"flag"
	"fmt"
	"io"
	"log"
	"net"
	"os"
)

func processClient(conn net.Conn) {
	_, err := io.Copy(os.Stdout, conn)
	if err != nil {
		fmt.Println(err)
	}
	conn.Close()
}

func startServer(host string, port int) {
	addr := fmt.Sprintf("%s:%d", host, port)

	listener, err := net.Listen("tcp", addr)
	if err != nil {
		panic(err)
	}

	log.Printf("Listening for connections from client: %s", listener.Addr().String())

	for {
		conn, err := listener.Accept()
		if err != nil {
			log.Printf("Error accepting connection from client: %s", err)
		} else {
			go processClient(conn)
		}
	}
}

func main() {
	portPtr := flag.Int("port", 8888, "port number to listen on")
	listenPtr := flag.Bool("listen", false, "Should you listen (server)?")
	hostPtr := flag.String("address", "0.0.0.0", "what address to listen on")
	flag.Parse()
	fmt.Println("port: ", *portPtr)
	fmt.Println("listen: ", *listenPtr)

	if *listenPtr {
		startServer(*hostPtr, *portPtr)
		return
	}
}
